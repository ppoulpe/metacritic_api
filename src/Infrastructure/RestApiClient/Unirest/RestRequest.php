<?php

declare(strict_types=1);

namespace App\Infrastructure\RestApiClient\Unirest;

use App\Core\Component\Parser\Port\RestRequestInterface;
use Unirest\Request;
use Unirest\Response;

class RestRequest implements RestRequestInterface
{
    /**
     * @param string $url
     *
     * @return Response
     */
    public function get(string $url): Response
    {
        return Request::get($url);
    }

    /**
     * @param bool $opt
     *
     * @return array
     */
    public function getInfo(bool $opt = false): array
    {
        return Request::getInfo($opt);
    }
}
