<?php

declare(strict_types=1);

namespace App\Infrastructure\Configuration;

use App\Core\Component\Parser\Port\ConfigurationInterface;
use Symfony\Component\Yaml\Yaml;

class ConfigurationYaml implements ConfigurationInterface
{
    public function getByFile(string $filename): array
    {
        return Yaml::parseFile($filename);
    }
}
