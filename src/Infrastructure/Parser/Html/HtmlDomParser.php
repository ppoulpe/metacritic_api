<?php

declare(strict_types=1);

namespace App\Infrastructure\Parser\Html;

use App\Core\Component\Parser\Port\ParserInterface;
use simplehtmldom_1_5\simple_html_dom;
use Sunra\PhpSimple\HtmlDomParser as SunraHtmlDomParser;

class HtmlDomParser implements ParserInterface
{
    public function getHtmlContent(string $body): simple_html_dom
    {
        return SunraHtmlDomParser::str_get_html($body);
    }

    public function find(simple_html_dom $source, string $selector)
    {
        return $source->find($selector);
    }
}
