<?php

declare(strict_types=1);

namespace App\Core\Component\Parser\ValueObject;

use ReflectionClass;

class Selectors
{
    const TITLE = 'title';
    const RELEASE_DATE = 'releaseDate';
    const DESCRIPTION = 'description';
    const GENRE = 'genre';
    const COVER_IMAGE = 'image';
    const SCORE = 'score';
    const DEVELOPER = 'developer';
    const PUBLISHER = 'publisher';
    const RATING = 'rating';
    const ALSO_AVAILABLE_ON = 'alsoAvailableOn';

    /**
     * @return array
     *
     * @throws \ReflectionException
     */
    public static function getConstantValues()
    {
        return array_values(
            (new ReflectionClass(self::class))
                ->getConstants()
        );
    }
}
