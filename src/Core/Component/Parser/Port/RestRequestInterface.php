<?php

declare(strict_types=1);

namespace App\Core\Component\Parser\Port;

interface RestRequestInterface
{
    public function get(string $url);

    public function getInfo(bool $opt = false): array;
}
