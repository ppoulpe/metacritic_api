<?php

declare(strict_types=1);

namespace App\Core\Component\Parser\Port;

interface ConfigurationInterface
{
    public function getByFile(string $filename): array;
}
