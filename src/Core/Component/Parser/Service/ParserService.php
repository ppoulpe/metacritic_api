<?php

namespace App\Core\Component\Parser\Service;

use App\Core\Component\Parser\Port\ConfigurationInterface;
use App\Core\Component\Parser\Port\ParserInterface;
use App\Core\Component\Parser\Port\RestRequestInterface;
use App\Core\SharedKernel\Helper\ArrayToolTrait;
use simplehtmldom_1_5\simple_html_dom;

class ParserService
{
    use ArrayToolTrait;

    const EMPTY_RESULT = 'No result';

    private $configuration;
    /**
     * @var ParserInterface
     */
    private $parser;
    /**
     * @var RestRequestInterface
     */
    private $restRequest;

    /**
     * ParserService constructor.
     *
     * @param ConfigurationInterface $configuration
     * @param ParserInterface        $parser
     * @param RestRequestInterface   $restRequest
     */
    public function __construct(
        ConfigurationInterface $configuration,
        ParserInterface $parser,
        RestRequestInterface $restRequest
    ) {
        $this->parser = $parser;
        $this->restRequest = $restRequest;
        $this->configuration = $configuration->getByFile(dirname(__DIR__).'/../../../../config/parsers/sources.yaml')['sources'];
    }

    /**
     * @param array $from
     * @param array $selectors
     *
     * @return array
     */
    public function get(
        array $from,
        array $selectors = ['title']
    ): array {
        if (empty($from['source'])) {
            return [];
        }

        $results = [];
        $url = !empty($from['params']) ? vsprintf($this->configuration[$from['source']]['url'], $from['params']) : $this->configuration[$from['source']]['url'];
        $rawBody = $this->parser->getHtmlContent(
            $this->restRequest->get($url)->raw_body
        );

        foreach ($selectors as $selector) {
            $results[$selector] = $this->getfromHtml(
                $rawBody,
                $this->configuration[$from['source']]['selectors'][$selector]['input'],
                $this->configuration[$from['source']]['selectors'][$selector]['output']
            );
        }

        return [
            'query' => $_SERVER['REQUEST_URI'],
            'executionTime' => $this->restRequest->getInfo()['total_time'],
            'result' => !$this->isNull($results) ? $results : self::EMPTY_RESULT,
        ];
    }

    /**
     * @param simple_html_dom $htmlDom
     * @param string $selector
     *
     * @param string $output
     * @return array
     */
    private function getfromHtml(
        simple_html_dom $htmlDom,
        string $selector,
        string $output = 'string'
    ) {
        $results = [];

        foreach (
            $this->parser->find(
                $htmlDom,
                $selector
            )
            as $element) {
            switch ($element->tag) {
                case 'img':
                    $results[trim($element->attr['alt'])] = trim($element->attr['src']);
                    break;
                default:
                    $results[] = trim($element->text());
                    break;
            }
        }

        if($output == 'array'){
            return $results;
        }

        $result = array_pop($results);
        settype($result, $output);

        return $result;
    }
}
