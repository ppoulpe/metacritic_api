<?php

declare(strict_types=1);

namespace App\Core\SharedKernel\Helper;

trait ArrayToolTrait
{
    /**
     * @param array $array
     * @param string $key
     * @return array|null
     */
    public function makeAssociativeArrayByKey(array $array, string $key): ?array
    {
        $current = 0;
        $associativeArrayByKey = [];
        $keys = array_keys($array);

        if(empty($array[$key])){
            return null;
        }

        while (count($array[$key]) > $current) {
            foreach ($keys as $key) {
                $associativeArrayByKey[$current][$key] = $array[$key][$current];
            }

            ++$current;
        }

        return $associativeArrayByKey;
    }

    /**
     * @param array $results
     * @return bool
     */
    public function isNull(array $arrays = [])
    {
        foreach($arrays as $key => $array){

            if(is_array($array)){
                return self::isNull($array);
            }

            if(!empty($array)){
                return false;
            }
        }

        return true;
    }
}
