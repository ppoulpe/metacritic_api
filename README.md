# Présentation
L'API Metacritic, comme son nom le suggère, vous permet de récupérer les informations d'un jeu à partir du site www.metacritic.com.

**Notez que l'API ne fait que retourner les informations d'un titre et ne stocke rien.**

# Prérequis
L'API fonctionne via `docker` et `docker-compose`. Il est donc impératif d'installer ces deux éléments :
* Installer docker : https://www.docker.com/get-started
* Installer docker-compose : https://docs.docker.com/compose/install/
# Installation
1. Construire et démarrer les containers (PHP + Apache) :
`docker-composer up -d`
2. Installer les dépendances du projet :
    * Se connecter en SSH sur le container PHP : `docker exec -it -u dev php bash` 
    *  Installer l'ensemble des librairies requises pour le projet : `composer install`

Vous devriez désormais avoir accès à l'API via votre localhost.
http://localhost

# Utilisation
Documentation disponible sur le site de l'API : [chicken-coop.fr](https://chicken-coop.fr)